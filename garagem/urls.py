from django.urls import path
from . import views

## URL's do aplicativo garagem
urlpatterns = [
    path('', views.main, name="main"), ## homepage
    path('car/<car>', views.car, name="car"), ## pagina de informação do carro
    path('new', views.new, name='new'), ## pagina de novos veiculos
    path('news', views.news, name="news"), ## pagina de novos veiculos2
    path('newg', views.newg, name="newg"), ## pagina de nova garagem
    path('newgs', views.newgs, name="newgs"), ## pagina de nova garagem
    path('register', views.register, name='register'), ## pagina de registro de novo usuário
    path('API/garages', views.apiGarage.as_view(), name='apigaragens'), ## pagina da API de garagens
    path('API/veiculos', views.apiVeiculos.as_view(), name='apiveiculos') ## pagina da API de garagens
]