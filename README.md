
## Aplicativo para registro e ocupação de garagens
Desenvolvimento de aplicativo para fins de avaliação da empresa Franq.

Objetivos:

1. Registro e autenticação de usuário (nome, telefone, e-mail)
2. Criação de "garagens" que informam: Modelo, cor e ano de fabricação de veículos registrados.
3. API disponível para uso de terceiros.
---

## Features:

Algumas características do aplicativo:

1. Uma garagem é criada automaticamente após registro de usuário;
2. Usuários podem cadastrar mais de um veículo em uma garagem;
3. Usuários podem adicionar uma garagem nova através da homepage;
4. Usuários podem visualizar informação do veículo na homepage;
5. Login e logout;
6. Criação de conta através da biblioteca django.contrib.auth;
7. API que fornece dados de garagens e seus respectivos donos;
8. API que fornece dados de veiculos e suas respectivas garagens;
9. Diferenciação entre consumidor e administrador (superuser - biblioteca django.contrib);
10. Painel administrador aravés da biblioteca django.contrib;

---

## Requerimentos:

1. pip install djangorestframework

---
