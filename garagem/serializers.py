from rest_framework import serializers
from .models import Garagem, Veiculos

class GarageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Garagem
        fields = '__all__'

class VeiculosSerializer(serializers.ModelSerializer):

    class Meta:
        model = Veiculos
        fields = '__all__'